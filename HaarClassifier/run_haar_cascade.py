import cv2 as cv

# Load cascade data
rad_cascade = cv.CascadeClassifier('Data/cascade.xml')

# Validation images
val_images = ['001.jpg', '002.jpg', '003.jpg', '004.jpg', '005.jpg', '006.jpg', '007.jpg', '008.jpg']

# Go through validation images
for val_image in val_images:
    # Load image to be analyzed
    img = cv.imread('Validation/' + val_image)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    # Analyze the image
    rads, rejectLevels, weightLevels = rad_cascade.detectMultiScale3(
        gray,
        scaleFactor = 1.3,
        minNeighbors = 5,
        minSize = (30, 30),
        outputRejectLevels = True
    )

    print('=============')
    print('rejectLevels: ')
    print(rejectLevels)
    print('weightLevels: ')
    print(weightLevels)


    # Show bounding boxes
    font = cv.FONT_HERSHEY_DUPLEX
    for (x,y,w,h) in rads:
        cv.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        cv.putText(img,'Radiator',(x,y-4),font,1,(0,255,0),1,cv.LINE_AA)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
    cv.imshow('img',img)

    # Wait for exit or next key
    key = cv.waitKey(0) & 0xFF
    if key == ord("q"):
        break

cv.destroyAllWindows()
