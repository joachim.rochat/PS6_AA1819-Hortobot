import numpy as np
import cv2 as cv
from imutils.video import VideoStream
from imutils.video import FPS
import imutils
import time

# Load cascade data
cascade = cv.CascadeClassifier('Data/cascade.xml')

# Load image to be analyzed
img = cv.imread('Validation/20190326_171300.jpg')
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

# Initialize the video stream and allow the camera sensor to warm up
vs = VideoStream(src=0).start()
# vs = VideoStream(usePiCamera=True).start()
time.sleep(1.0)

# Start the FPS counter
fps = FPS().start()

# Loop over frames from the video file stream
while True:
    # Get a frame from the video stream
    frame = vs.read()
    frame = imutils.resize(frame, width=500)

    # Convert to grayscale for detection
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    # Detect objects in the frame
    objects = cascade.detectMultiScale(gray, scaleFactor=1.3,minNeighbors=5,minSize=(30, 30),flags=cv.CASCADE_SCALE_IMAGE)

    # Show bounding boxes
    for (x,y,w,h) in objects:
        cv.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        cv.putText(img,'Block',(x,y-4),cv.FONT_HERSHEY_DUPLEX,1,(0,255,0),1,cv.LINE_AA)
        # roi_gray = gray[y:y+h, x:x+w]
        # roi_color = img[y:y+h, x:x+w]

    # Display the image to our screen
    cv.imshow("Frame", frame)
    key = cv.waitKey(1) & 0xFF

    # If the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

    # Update the FPS counter
    fps.update()

# Timer stop
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# Cleanup
cv.destroyAllWindows()
vs.stop()
