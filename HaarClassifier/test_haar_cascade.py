import cv2 as cv

# Load cascade data
rad_cascade = cv.CascadeClassifier('Data/cascade.xml')

# Load image to be analyzed
img = cv.imread('Validation/003.jpg')
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

# Analyze the image
rads = rad_cascade.detectMultiScale(
    gray,
    scaleFactor = 1.3,
    minNeighbors = 5
)

# Show bounding boxes
font = cv.FONT_HERSHEY_DUPLEX
for (x,y,w,h) in rads:
    cv.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
    cv.putText(img,'Radiator',(x,y-4),font,1,(0,255,0),1,cv.LINE_AA)
    roi_gray = gray[y:y+h, x:x+w]
    roi_color = img[y:y+h, x:x+w]
cv.imshow('img',img)

# Wait for exit
key = cv.waitKey(0) & 0xFF

cv.destroyAllWindows()
