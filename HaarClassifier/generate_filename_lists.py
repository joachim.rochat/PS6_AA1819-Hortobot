from datetime import datetime
import glob
from PIL import Image

##
## NEGATIVE SAMPLES LIST
##

# Start time
start_time = datetime.now()

NEG_BASE_PATH = 'Negative/Frames/'
NEG_EXTENSION = '*.jpg'
NEG_LIST_FILENAME = 'bg.txt'

# Get negative samples filenames
neg_dir_path = NEG_BASE_PATH + NEG_EXTENSION
neg_filenames = glob.glob(neg_dir_path)

# Generate filenames list
neg_filenames = [filename for filename in neg_filenames]
neg_filenames_str = '\r\n'.join(map(str, neg_filenames))
neg_filenames_str = neg_filenames_str.replace('\\','/')

# Write the list to a file
neg_file = open(NEG_LIST_FILENAME, 'w')
neg_file.write(neg_filenames_str)
neg_file.close()

# Print success
exec_time = str(datetime.now() - start_time)
print(str(len(neg_filenames)) + ' negatives pictures were listed in ' + exec_time)

##
## POSITIVE SAMPLES LIST
##

# Start time
start_time = datetime.now()

POS_BASE_PATH = 'Positive/'
POS_EXTENSION = '*.jpg'
POS_LIST_FILENAME = 'info.dat'

# Get posative samples filenames
pos_dir_path = POS_BASE_PATH + POS_EXTENSION
pos_filenames = glob.glob(pos_dir_path)

# Generate bounding box data for already cropped images
pos_bounding = []
for pos_file in pos_filenames:
    with Image.open(pos_file) as img:
        width, height = img.size
        pos_bounding.append(' 1 0 0 ' + str(width) + ' ' + str(height))

# Generate filenames list
pos_filenames = [fnm + bnd for fnm, bnd in zip(pos_filenames, pos_bounding)]
pos_filenames_str = '\r\n'.join(map(str, pos_filenames))
pos_filenames_str = pos_filenames_str.replace('\\','/')

# Write the list to a file
pos_file = open(POS_LIST_FILENAME, 'w')
pos_file.write(pos_filenames_str)
pos_file.close()

# Print success
exec_time = str(datetime.now() - start_time)
print(str(len(pos_filenames)) + ' positive pictures were listed in ' + exec_time)
