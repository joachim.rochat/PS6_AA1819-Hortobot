#!/bin/bash

# Elevate privileges
if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

# Set color constants
RED='\033[0;31m'
NC='\033[0m' # No Color

echo -e "${RED}Setup environment for tensorflow models${NC} "
apt-get update && apt-get install -y \
        build-essential \
        curl \
        git \
        libfreetype6-dev \
        libpng-dev \
        libzmq3-dev \
        pkg-config \
        python-dev \
        python-numpy \
        python-pip \
        protobuf-compiler \
        python-pil \
        python-lxml \
        python-tk \
        software-properties-common \
        swig \
        zip \
        zlib1g-dev \
        libcurl3-dev \
        && \
    apt-get clean && \
rm -rf /var/lib/apt/lists/*

echo -e "${RED}Install Python${NC} "
apt-get install python-pip python-dev build-essential

echo -e "${RED}Set up grpc${NC} "
pip install enum34 futures mock six matplotlib jupyter && \
pip install --pre 'protobuf>=3.0.0a3' && \
pip install -i https://testpypi.python.org/simple --pre grpcio

echo -e "${RED}Done${NC} "










