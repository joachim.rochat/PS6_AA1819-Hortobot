# Installer python3-dev pour avoir les header lors du make
sudo apt-get update
sudo apt-get install python3-dev

# Créer un nouvel environnement virtuel
mkvirtualenv -p python3 tf

# Lors des nouvelles sessions bash, travailler dessus avec
workon tf

# Installer tensorflow avec pip
pip install --upgrade tensorflow

# Vérifier l'installation avec
python -c "import tensorflow as tf; tf.enable_eager_execution(); print(tf.reduce_sum(tf.random_normal([1000, 1000])))"

# Télécharger les les modèles
cd ~
git clone https://github.com/tensorflow/models.git
git clone https://github.com/cocodataset/cocoapi.git
cd cocoapi/PythonAPI 
pip install Cython && make
cp -r pycocotools ~/models/research/


cd /models/research/
protoc object_detection/protos/*.proto --python_out=.
PYTHONPATH=$PYTHONPATH:/models/research/:/models/research/slim



